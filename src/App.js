import React from 'react';
import './App.css';
// import Example1 from '../src/components/Example1';
// import Example2 from '../src/components/Example2';
import Example3 from '../src/components/Example3';

function App() {
  return (
    <div>
        {/* Showing Data in Console */}
        {/* <Example1></Example1> */}
        {/*  Showing Table Dessing with Boostrap */}
				{/* <Example2></Example2> */}
        {/* Showing table with data, iterating trough Json, gettint Header and Body */}
				<Example3></Example3>
    </div>
  );
}

export default App;