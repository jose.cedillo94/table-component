import React, { Component } from 'react';
import data from "../data/data"; 

class Example1 extends Component {
  render() {
		return (
            <div>
              <p>Please go to Console with Ctrl + Shift + i</p>
              {console.log(data)}
            </div>
        );
    }
} 

export default Example1;